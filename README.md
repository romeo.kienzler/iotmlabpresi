Presi folder

To run:  
docker build -t presi .  
docker run -it --rm -p 8000:8000 presi  

To develop:  
git clone git@gitlab.com:ansgarschmidt/iotmlabpresi.git  
cd iotmlabpresi  
git clone https://github.com/hakimel/reveal.js.git  
cd reveal.js  
ln -s ../satnogs.html .  
ln -s ../index.html .  
ln -s ../pic .  

edit files in iotmlabpresi folder and open in browser the file in the reveal.js folder

